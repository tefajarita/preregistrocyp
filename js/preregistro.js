function cargarCiudades(){
	var url = "data/ciudades.json";
	var TAB = "\t";
	
	$.getJSON(url, function (data) {
		$.each(data.ciudades, function (index, value) {
			if(value.MOSTRAR === true){
				$('#city').append('<option value="' + value.CIUDAD__C + '">' + value.CIUDAD__C + TAB + '(' + value.NOMBRE_ESTADO__C +')</option>');
			}
		});
	});
}